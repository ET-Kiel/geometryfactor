#! /usr/bin/env ./geometryfactor.py
# CHAOS Jr.  HET without A-detectors

φ1=0
φ2=π/6
Δr=0.1
Δθ=0.5°

#BGO
define=W=20
define=D=40
prism= 6, ${D}/2*sqrt(3/4), ${W}/2

# SSDs
define=a=24.3
define=w=0.3
define=d=36.5
define=n=36

prism= ${n}, ${d}/2, ${w}/2
move= 0,0,-${a}/2
copy=-1
move= 0,0,${a}

θ₂=atan2(${d},${a}+${w})
radius=sqrt(${d}**2 + (${a}+${w})**2)/2
