#! /usr/bin/make -f

RUN=ahepam-1mm_1°
CUTS=H A B AB BC AC  AB1 AnBn  AB1~C AnBn~C  BC1 A1C BnCn AnCm  BC1~D A1C~D BnCn~D AnCm~D

all: $(patsubst %, $(RUN)_%.ssv, $(CUTS))

# Ray C D B E A G F
# 4   8 8 8 7 7 1 1

# A 36 31  C H H H H H H
# B 21 16  C H H H H H H R
# C  5  0  C H H H H H H R
# D 13  8  C H H H H H H R
# E 29 24  C H H H H H H
# G 43 38  BGO
# F 44 39  BGO

# exactly one ' ' between path lengths
# two space between ray path length
# four columns 1..4 for the ray 

# R segments are never part of a coincidence,
#   but always part of an anticoincidence.

%_H.ssv: %.ssv
	egrep -v ' ( 0){40}$$' $< > $@

%_A.ssv: %_H.ssv
	egrep -v '  ([-+.e0-9]+ ){31}(0 ){7}' $< > $@

%_AB.ssv: %_A.ssv
	egrep -v '  ([-+.e0-9]+ ){16}(0 ){7}' $< > $@

%_B.ssv: %_H.ssv
	egrep -v '  ([-+.e0-9]+ ){16}(0 ){7}' $< > $@

%_BC.ssv: %_B.ssv
	egrep -v '  (0 ){7}' $< > $@

%_AC.ssv: %_A.ssv
	egrep -v '  (0 ){7}' $< > $@

# Stopping in BGO1

%_AB1.ssv: %_AB.ssv
	egrep -v '  ([-+.e0-9]+ ){16}0 ' $< > $@

%_AnBn.ssv: %_AB.ssv
	egrep    '  ([-+.e0-9]+ ){16}0 ' $< \
      | awk '$$37&&$$22||$$38&&$$23||$$39&&$$24||$$40&&$$25||$$41&&$$26||$$42&&$$27' \
      > $@

%_AB1~C.ssv: %_AB1.ssv
	-egrep '  (0 ){8}' $< > $@

%_AnBn~C.ssv: %_AnBn.ssv
	-egrep '  (0 ){8}' $< > $@

# Stopping in BGO2

%_BC1.ssv: %_BC.ssv
	egrep -v '  0 ' $< > $@

%_A1C.ssv: %_BC.ssv
	egrep    '  0 ' $< \
      | egrep -v '  ([-+.e0-9]+ ){31}0 ' \
      > $@

%_BC~A1~C1.ssv: %_BC.ssv
	egrep '  0 ([-+.e0-9]+ ){30}0 ' $< > $@

%_BnCn.ssv: %_BC.ssv
	egrep '  0 ([-+.e0-9]+ ){30}0 ' $< \
      | awk '$$6&&$$22||$$7&&$$23||$$8&&$$24||$$9&&$$25||$$10&&$$26||$$11&&$$27' \
      > $@

%_AnCm.ssv: %_BC.ssv
	egrep '  0 ([-+.e0-9]+ ){30}0 ' $< \
      | awk   '$$6&&$$38||$$7&&$$39||$$8&&$$40||$$9&&$$41||$$10&&$$42||$$11&&$$37 \
             ||$$6&&$$42||$$7&&$$37||$$8&&$$38||$$9&&$$39||$$10&&$$40||$$11&&$$41' \
      | awk '!($$6&&$$22||$$7&&$$23||$$8&&$$24||$$9&&$$25||$$10&&$$26||$$11&&$$27)' \
      > $@

%_BC1~D.ssv: %_BC1.ssv
	-egrep '  ([-+.e0-9]+ ){8}(0 ){8}' $< > $@
%_A1C~D.ssv: %_A1C.ssv
	-egrep '  ([-+.e0-9]+ ){8}(0 ){8}' $< > $@
%_BnCn~D.ssv: %_BnCn.ssv
	-egrep '  ([-+.e0-9]+ ){8}(0 ){8}' $< > $@
%_AnCm~D.ssv: %_AnCm.ssv
	-egrep '  ([-+.e0-9]+ ){8}(0 ){8}' $< > $@
