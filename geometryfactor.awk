#! /usr/bin/gawk -i

function mult() {
    nHITS=0
    for (i=5; i<=NF; i++) 
	if ($i > 0) {
	    TLEN[nHITS] = $i
	    HITS[nHITS++] = i-5
	}
    for (i=0; i<nHITS-1; i++) 
	for (j=i+1; j<nHITS; j++)
	    if (TLEN[i] < TLEN[j]) {
		a=HITS[j]
		HITS[j]=HITS[i]
		HITS[i]=a
		a=TLEN[j]
		TLEN[j]=TLEN[i]
		TLEN[i]=a
	    }
    return nHITS
}

BEGIN {
    pi = 4*atan2(1,1)
    deg = pi/180
    res_xy = 0.5*deg
}

function hasDZ() {
    return mult()>=2 && and(xor(HITS[0], HITS[1]), 16)
}

function Y(c) { return and(c,4)/2  + and(c,1)   - 1.5 }
function X(c) { return and(c,8)/4  + and(c,2)/2 - 1.5 }
function Z(c) { return and(c,16) }
    
#
# Mirror φ=0…π/2 to the full circle.
# print -z first
#

function doCoinc1(f, phi, c0, c1) {
    if (Z(c1)) @f($1, phi, c0, c1, TLEN[0], TLEN[1])
    else       @f($1, phi, c1, c0, TLEN[1], TLEN[0])
}

function doCoinc(f) {
    doCoinc1(f, $2,        HITS[0],         HITS[1]    )
    if (!$1) return
    doCoinc1(f, pi-$2, xor(HITS[0],10), xor(HITS[1],10))
    doCoinc1(f,   -$2, xor(HITS[0], 5), xor(HITS[1], 5))
    doCoinc1(f, pi+$2, xor(HITS[0],15), xor(HITS[1],15))
}

function printCoinc(theta, phi, c0, c1, l0, l1) {
    print $1, phi, X(c1)-X(c0), Y(c1)-Y(c0), c0, c1, l0, l1
}

BEGIN {
    for (i=0; i<32; i++)
	for (j=0; j<32; j++)
	    HKEYxy[i,j] = sprintf("C%+d%+d", X(j)-X(i), Y(j)-Y(i))
}

function histxyCoinc(theta, phi, c0, c1, l0, l1) {
    x = int(sin(theta)*cos(phi)/res_xy+1000.5)-1000
    y = int(sin(theta)*sin(phi)/res_xy+1000.5)-1000
    k = HKEYxy[c0,c1]
    if (!iHIST2D) {
	min_x = x
	max_x = x
	min_y = y
	max_y = y
    } else {
	if (x>max_x) max_x = x
	if (x<min_x) min_x = x
	if (y>max_y) max_y = y
	if (y<min_y) min_y = y
    }
    iHIST2D++
    HIST2DKEY[k]++
    HIST2D[k,x,y]++
}

END { if (iHIST2D) emit_HIST2D() }

function emit_HIST2D() {
    n=0;
    for (k in HIST2DKEY) kk[n++] = k
    for (i=0; i<n-1; i++) 
	for (j=i+1; j<n; j++)
	    if (kk[i] > kk[j]) {
		k=kk[j]
		kk[j]=kk[i]
		kk[i]=k
	    }
    printf "x y "
    for (i=0; i<n; i++) printf " %s", kk[i]
    printf "\n"
    for (x=min_x; x<=max_x; x++) {
	for (y=min_y; y<=max_y; y++) {
	    printf "%.3f %.3f ", x*res_xy, y*res_xy
	    for (i=0; i<n; i++)
		printf " %d", HIST2D[kk[i],x,y]+0
	    printf "\n"
	}
	printf "\n"
    }
}
