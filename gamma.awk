#! /usr/bin/gawk -f

BEGIN {
    gamma = 1
    thres = 10
}

!/^#/ && NF==3 {
    w = cos($1)
    if (w>0) w = $3 * exp(log(w)*gamma)
    else w = 0
    l = $2
    L[l] += w
    if (l>=thres) N += w
}

END {
    print "#", N
    for (l in L) print l, L[l] | "sort -n"
}
