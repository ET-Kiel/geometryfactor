Integrate geometry factors of convex polyhedrons
================================================
The world can contain multiple detectors.  Detectors are defined as
sets of planes.  The pathlengths of the intersection of rays with the
detectors are computed.

The rays are generated in a range of angles θ₁…θ₂ from the *z*-axis in
steps Δθ, and a range of angles φ₁…φ₂ around the *z*-axis with density
1/Δθ².

For each direction a beam of radius *r* is generated, with parallel
rays on circles around the ray through the origin separated by Δ*r*
and density 1/Δ*r*².

Data model
----------
A plane is defined by a point and two vectors, `plane(o,v,w)`.
The cross product `n=v×w` points inside the detector.

A ray is defined by a point and a vector `ray().ov(o,v)`.  The
constructor of a ray() takes four arguments `ray(θ, φ, r, ψ)` and
computes the point and vector.  (*r*,ψ) define a point in the
*x*,*y*-plane, that is rotated by θ around the *y*-axis and then by φ
around the *z*-axis.  The ray goes through the rotated point
perpendicular to the rotated *x*,*y*-plane.

Running the script
------------------
The script `geometryfactor.py` is run with commandline options.  Any
non-options are files containing more options.

Each line in a file is treated as a further commandline argument,
except for empty lines and lines starting with `#`.  Lines starting
with a `-` are used as they are. Lines starting with a `@` are used
with the `@` stripped off.  All other lines get `--` prefixed.

Action options
--------------
These actions are executed after all other options were applied, in
this order:

- `-E` `--corners`: find all corners of all detectors.
- `-S` `--size`: compute the size of the integration run.
- `-T` `--test`: run the test rays through the world.
- `-R` `--run`: perform the integration.
- `-G` `--gnuplot`: output data to splot the world with gnuplot.

Beam options
------------
Option values are evaluated as python expressions. `°` is translated
as `*deg`.  `math.pi` is imported as `π`.  Macros are expanded,
recursively.

- `-a` `--Δθ=`Δθ: angular resolution (1°)
- `-d` `--Δr=`Δr: spacial resolution (1)
- `--θ1=`θ₁: θ starting angle (0)
- `--θ2=`θ₂: θ stopping angle (π/2)
- `--φ1=`φ₁: φ starting angle (0)
- `--φ2=`φ₂: φ stopping angle (π/4)
- `-r` `--radius=`*r*: beam radius (0)

Defaults are given in parenthesis. `--radius=0` implies `--corners` and
sets the beam radius to the radius of the world.

World options
-------------
Points or vectors are space or comma separated expressions evaluated
as explained in section _beam options_.  Multiple vectors are
separated by `;`.

### Creating detectors

- `-N` `--new`: Append a new empty detector to the world.
- `-B` `--box=`*x*,*y*,*z*: Append a new rectangular box detector to
  the world. The dimensions are the half-widths of the box, i.e., the
  coordinates of the corners.
- `C` `--copy=`*n*: Append a copy of detector number *n* to the
  world. (python list index)
- `--select=`*n*: Move detector number *n* to the end of the world.
- `-µ` `--µMustang`: Append a µMustang Detector to the end of the
  world. 

Empty detectors are not retained, except at the end of the world
during genesis.  When no detectors are given, `-µ` is implied.

### Detector manipulation

Detector manipulation always affects the last detector of the world.

- `-p` `--plane=`*o*;*v*;*w*: Add a plane to the detector.
- `--move=`*v*: Move the detector by the vector *v*.
- `--scale=`*v*: Scale the detector by factors given for each axis.
- `--rx=`φ: Rotate the detector by the angle φ around the *x*-axis,
- `--ry=`φ: *y*-axis,
- `--rz=`φ: or *z*-axis.

Test beam
---------
Three rays along the coordinate axes are defined for the
execution of `--test`.  Further rays may be given by

- `-x` `--ray=`*name*;*o*;*v*: with point *o* and vector *v*, or
- `-x` `--ray=`*name*;θ,φ,r,ψ: with direction and offset.

Macros
------
Macros are simple textual substitutions in expressions evaluated as
part of option arguments.  Strings of the form `${`*macro*`}` are
replaced by the definition of *macro*.

- `-D` `--define=`*macro*`=`*text*:  Define *macro* unless it is
  already defined.
- `-U` `--undefine=`*macro*: Undefine *macro*.
- `--replace=`*macro*`=`*text*: Define or redefine *macro*.

Output
------
The output of `--corners`, `--size`, and `--test` goes to _stderr_.

`--run` produces one line of output on _stdout_ for each ray sent
through the world.  The columns are: θ, φ, r, ψ, and the intersection
length for each detector in the world.

`--gnuplot` output goes to _stdout_.
