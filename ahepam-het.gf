#! /usr/bin/env ./geometryfactor.py
# AHEPAM w/ HETA/HETB detectors.

φ1=0
φ2=π/12
Δr=1
Δθ=1°
radius=65

define=a=4
define=a₁=${a}/sqrt(3)
define=a₂=${a}*tan(π/12)
define=r₁=2/sqrt(3)*${a}
define=b=8.7
define=b₁=${b}*cos(π/12)
define=b₂=${b}*sin(π/12)
define=c=18.25
define=c₀=${c}/sqrt(2)
define=c₁=${c}*cos(π/6)
define=c₂=${c}*sin(π/6)
define=c₃=${c}*cos(π/12)
define=c₄=${c}*sin(π/12)
define=d=30.0
define=dd=50.0
define=w=1.0
define=v=(${d}-10)
define=e=39.0

#radius=sqrt((2*${d}+${w})**2 + 2*${b}**2)

# center segment
prism= 24, ${a}, ${w}/2

# ring segment
prism= 24, ${b}, ${w}/2

# outer segment
prism= 24, ${c}, ${w}/2

copy=0
move=0,0,${d}
copy=1
move=0,0,${d}
copy=2
move=0,0,${d}

copy=0
move=0,0,-${d}
copy=1
move=0,0,-${d}
copy=2
move=0,0,-${d}

copy=0
move=0,0, ${d}+${dd}
copy=1
move=0,0, ${d}+${dd}
copy=2
move=0,0, ${d}+${dd}

copy=0
move=0,0, -${d}-${dd}
copy=1
move=0,0, -${d}-${dd}
copy=2
move=0,0, -${d}-${dd}

# BGO

prism= 6, ${e}, ${v}/2
move=0,0,${d}/2

copy=-1
move=0,0,-${d}

# Cherenkov

box= 31, 31, 20
move= 0,0, ${d}+${dd}/2

copy=-1
move= 0,0, -2*${d}-${dd}
