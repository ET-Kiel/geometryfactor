#! /usr/bin/env ./geometryfactor.py
# Mustang 
φ1=0
φ2=π/2
Δr=10
Δθ=0.5°

# length units are mm, distances are center-center

# dimensions of the Scintillators
define=wx=500.
define=wy=${wx}
define=wz=50.
define=αy=6°

# position of the szintillators inside the boxs
define=ox=523.
define=oy=510.
define=oz=0

# position of the boxes
define=px=1150.
define=py=${px}
define=pz=1155.

#                        +y
#          __    __               __    __
#     __---  |  |  ---__  ^  __---  |  |  ---__
#    |    21 |  |    23 | | |    29 |  |    31 |
#    |       |  |       | | |       |  |       |
#    |  5    |  |  7    | | | 13    |  | 15    |
#    |     __|  |__     | | |     __|  |__     |
#    |__---        ---__| | |__---        ---__|
#          __    __       |       __    __
#     __---  |  |  ---__  |  __---  |  |  ---__
#    |    20 |  |    22 | | |    28 |  |    30 |
#    |       |  |       | | |       |  |       |
#    |  4    |  |  6    | | | 12    |  | 14    |
#    |     __|  |__     | | |     __|  |__     |
#    |__---        ---__| | |__---        ---__|
#  -----------------------+----------------------> +x
#          __    __       |       __    __
#     __---  |  |  ---__  |  __---  |  |  ---__
#    |    17 |  |    19 | | |    25 |  |    27 |
#    |       |  |       | | |       |  |       |
#    |  1    |  |  3    | | |  9    |  | 11    |
#    |     __|  |__     | | |     __|  |__     |
#    |__---        ---__| | |__---        ---__|
#          __    __       |       __    __
#     __---  |  |  ---__  |  __---  |  |  ---__
#    |    16 |  |    18 | | |    24 |  |    26 |
#    |   (+z)|  |       | | |       |  |       |
#    |  0    |  |  2    | | |  8    |  | 10    |
#    | (-z)  |  |__     | | |     __|  |__     |
#    |__---        ---__| | |__---        ---__|
#                         |


box=${wx}/2 ${wy}/2 ${wz}/2
ry=${αy}
move=-${ox}/2-${px}/2,-${oy}/2-${py}/2,-${oz}/2-${pz}/2
copy=-1
move=0,${oy},0

box=${wx}/2 ${wy}/2 ${wz}/2
ry=-${αy}
move=${ox}/2-${px}/2,-${oy}/2-${py}/2,-${oz}/2-${pz}/2
copy=-1
move=0,${oy},0

copy=0
move=0,${py},0
copy=1
move=0,${py},0
copy=2
move=0,${py},0
copy=3
move=0,${py},0

copy=0
move=${px},0,0
copy=1
move=${px},0,0
copy=2
move=${px},0,0
copy=3
move=${px},0,0
copy=4
move=${px},0,0
copy=5
move=${px},0,0
copy=6
move=${px},0,0
copy=7
move=${px},0,0

copy=0
move=0,0,${pz}
copy=1
move=0,0,${pz}
copy=2
move=0,0,${pz}
copy=3
move=0,0,${pz}
copy=4
move=0,0,${pz}
copy=5
move=0,0,${pz}
copy=6
move=0,0,${pz}
copy=7
move=0,0,${pz}
copy=8
move=0,0,${pz}
copy=9
move=0,0,${pz}
copy=10
move=0,0,${pz}
copy=11
move=0,0,${pz}
copy=12
move=0,0,${pz}
copy=13
move=0,0,${pz}
copy=14
move=0,0,${pz}
copy=15
move=0,0,${pz}

ray=z00; -${ox}/2-${px}/2, -${oy}/2-${py}/2;    0,0,1
ray=z01; -${ox}/2-${px}/2, +${oy}/2-${py}/2;    0,0,1
ray=z02; +${ox}/2-${px}/2, -${oy}/2-${py}/2;    0,0,1
ray=z03; +${ox}/2-${px}/2, +${oy}/2-${py}/2;    0,0,1
ray=z10; -${ox}/2-${px}/2, -${oy}/2+${py}/2;    0,0,1
ray=z11; -${ox}/2-${px}/2, +${oy}/2+${py}/2;    0,0,1
ray=z12; +${ox}/2-${px}/2, -${oy}/2+${py}/2;    0,0,1
ray=z13; +${ox}/2-${px}/2, +${oy}/2+${py}/2;    0,0,1
ray=z20; -${ox}/2+${px}/2, -${oy}/2-${py}/2;    0,0,1
ray=z21; -${ox}/2+${px}/2, +${oy}/2-${py}/2;    0,0,1
ray=z22; +${ox}/2+${px}/2, -${oy}/2-${py}/2;    0,0,1
ray=z23; +${ox}/2+${px}/2, +${oy}/2-${py}/2;    0,0,1
ray=z30; -${ox}/2+${px}/2, -${oy}/2+${py}/2;    0,0,1
ray=z31; -${ox}/2+${px}/2, +${oy}/2+${py}/2;    0,0,1
ray=z32; +${ox}/2+${px}/2, -${oy}/2+${py}/2;    0,0,1
ray=z33; +${ox}/2+${px}/2, +${oy}/2+${py}/2;    0,0,1

ray=x00; 0, -${oy}/2-${py}/2, -${oz}/2-${pz}/2; 1,0,0
ray=x01; 0, +${oy}/2-${py}/2, -${oz}/2-${pz}/2; 1,0,0
ray=x02; 0, -${oy}/2+${py}/2, -${oz}/2-${pz}/2; 1,0,0
ray=x03; 0, +${oy}/2+${py}/2, -${oz}/2-${pz}/2; 1,0,0
ray=x10; 0, -${oy}/2-${py}/2, -${oz}/2+${pz}/2; 1,0,0
ray=x11; 0, +${oy}/2-${py}/2, -${oz}/2+${pz}/2; 1,0,0
ray=x12; 0, -${oy}/2+${py}/2, -${oz}/2+${pz}/2; 1,0,0
ray=x13; 0, +${oy}/2+${py}/2, -${oz}/2+${pz}/2; 1,0,0

ray=y00; -${ox}/2-${px}/2, 0, -${oz}/2-${pz}/2; 0,1,0
ray=y01; +${ox}/2-${px}/2, 0, -${oz}/2-${pz}/2; 0,1,0
ray=y02; -${ox}/2+${px}/2, 0, -${oz}/2-${pz}/2; 0,1,0
ray=y03; +${ox}/2+${px}/2, 0, -${oz}/2-${pz}/2; 0,1,0
ray=y10; -${ox}/2-${px}/2, 0, -${oz}/2+${pz}/2; 0,1,0
ray=y11; +${ox}/2-${px}/2, 0, -${oz}/2+${pz}/2; 0,1,0
ray=y12; -${ox}/2+${px}/2, 0, -${oz}/2+${pz}/2; 0,1,0
ray=y13; +${ox}/2+${px}/2, 0, -${oz}/2+${pz}/2; 0,1,0

