#! /usr/bin/gawk -f

BEGIN {
    Z=0
}

/^[0-9]/&& NF>=4 {
    theta = -$1+0
    phi = $2+0
    r = $3+0
    psi = $4+0
    x0 = r*cos(psi)
    y0 = r*sin(psi)
    # rotate by -theta around y
    costh = cos(theta)
    sinth = sin(theta)
    xx1 =  x0*costh
    xx2 =  x0*costh
    z1 =  -x0*sinth
    z2 =  -x0*sinth
    # rotate by phi around z
    cosphi = cos(phi)
    sinphi = sin(phi)
    x1 =  xx1*cosphi - y0*sinphi
    x2 =  xx2*cosphi - y0*sinphi
    y1 =  xx1*sinphi + y0*cosphi
    y2 =  xx2*sinphi + y0*cosphi
    print x1, y1, z1
    print x2, y2, z2
    print Nix
    print Nix
}
