#! /usr/bin/gawk -f

BEGIN {
    dr = 1
    r_max = 100
    theta = 9999
}

$1 != theta {
    if (n) emit()
    theta = $1
}

{
    i = int($5/dr+0.5)
    N[i]++
    n++
    if (i) m++
}

function emit() {
    nr = r_max/dr+1
    print "#", n, m
    for (i=0; i<nr; i++) print theta, i*dr, N[i]+0
    print ""
    delete N
    n = 0
}

END { if (n) emit() }
