# AHEPAM honey

φ1=0
φ2=π/6
Δr=1
Δθ=1°
radius=65

define=a=8.0
define=a₁=${a}/sqrt(3)
define=r₁=2/sqrt(3)*${a}
define=b=${a}*sqrt(21/6/sqrt(3)/tan(π/12))
define=b₁=${b}*cos(π/6)
define=b₂=${b}*sin(π/6)
define=Δc=2
define=c=(2*${b}-${a}+${Δc})
define=c₁=${c}*cos(π/6)
define=c₂=${c}*sin(π/6)
define=d=30.0
define=w=1.0
define=v=(${d}-10)
define=e=(${c}-${a})

#radius=sqrt((2*${d}+${w})**2 + 2*${b}**2)

# center segment
detector
plane=    0, 0,-${w}/2;   1., 0, 0;   0, 1., 0
plane=    0, 0, ${w}/2;   0, 1., 0;   1., 0, 0
plane=   -${a}, 0, 0;   0, 1., 0;   0, 0,  1.
plane=    ${a}, 0, 0;   0, 0,  1.;  0, 1., 0
plane=  0, -${r₁}, 0;  0, 0,  1.;  ${a},  ${a₁}, 0
plane=  0, -${r₁}, 0;  0, 0,  1.;  ${a}, -${a₁}, 0
plane=  0,  ${r₁}, 0;  ${a},  ${a₁}, 0;  0, 0,  1.
plane=  0,  ${r₁}, 0;  ${a}, -${a₁}, 0;  0, 0,  1.

# hex segment
detector
plane=    0, 0,-${w}/2;   1., 0, 0;   0, 1., 0
plane=    0, 0, ${w}/2;   0, 1., 0;   1., 0, 0
plane=    ${a}, 0, 0;   0, 1., 0;   0, 0,  1.
plane=    ${a}, -${a₁}, 0;  0, 0,  1.;  ${a}, -${a₁}, 0
plane=    ${a},  ${a₁}, 0;  ${a},  ${a₁}, 0;  0, 0,  1.
plane=    ${b}, 0, 0;   0, 0,  1.;  0, 1., 0
plane=    ${b₁}, -${b₂}, 0;  0, 0,  1.;  ${b₂}, ${b₁}, 0
plane=    ${b₁},  ${b₂}, 0;  ${b₂}, -${b₁}, 0;  0, 0,  1.

copy=-1
rz=π/3
copy=-2
rz=2*π/3
copy=-3
rz=π
copy=-4
rz=4*π/3
copy=-5
rz=5*π/3

# outer segment
detector
plane=   0, 0,-${w}/2;   1., 0, 0;   0, 1., 0
plane=   0, 0, ${w}/2;   0, 1., 0;   1., 0, 0
plane=  -${c}, 0, 0;   0, 1., 0;   0, 0,  1.
plane=   ${c}, 0, 0;   0, 0, 1.;   0, 1., 0
plane=  0, -${c}, 0;   0, 0, 1.;   1., 0, 0
plane=  0,  ${c}, 0;   1., 0, 0;   0, 0,  1.

plane=  -${c₁},  ${c₂}, 0;   ${c₂}, ${c₁}, 0;   0, 0,  1.
plane=  -${c₁}, -${c₂}, 0;  -${c₂}, ${c₁}, 0;   0, 0,  1.
plane=   ${c₁},  ${c₂}, 0;   0, 0, 1.;  -${c₂}, ${c₁}, 0
plane=   ${c₁}, -${c₂}, 0;   0, 0, 1.;   ${c₂}, ${c₁}, 0

plane=  ${c₂}, -${c₁}, 0;   0, 0, 1.;   ${c₁},  ${c₂}, 0
plane= -${c₂}, -${c₁}, 0;   0, 0, 1.;   ${c₁}, -${c₂}, 0
plane=  ${c₂},  ${c₁}, 0;   ${c₁}, -${c₂}, 0;   0, 0,  1.
plane= -${c₂},  ${c₁}, 0;   ${c₁},  ${c₂}, 0;   0, 0,  1.


copy=-8
move=0,0,${d}
copy=-8
move=0,0,${d}
copy=-8
move=0,0,${d}
copy=-8
move=0,0,${d}
copy=-8
move=0,0,${d}
copy=-8
move=0,0,${d}
copy=-8
move=0,0,${d}
copy=-8
move=0,0,${d}

# define=Ar=π/6
define=Ar=0
copy=-16
move=0,0,-${d}
rz=${Ar}
copy=-16
move=0,0,-${d}
rz=${Ar}
copy=-16
move=0,0,-${d}
rz=${Ar}
copy=-16
move=0,0,-${d}
rz=${Ar}
copy=-16
move=0,0,-${d}
rz=${Ar}
copy=-16
move=0,0,-${d}
rz=${Ar}
copy=-16
move=0,0,-${d}
rz=${Ar}
copy=-16
move=0,0,-${d}
rz=${Ar}

copy=0
move=0,0, 2*${d}
copy=1
move=0,0, 2*${d}
copy=2
move=0,0, 2*${d}
copy=3
move=0,0, 2*${d}
copy=4
move=0,0, 2*${d}
copy=5
move=0,0, 2*${d}
copy=6
move=0,0, 2*${d}

copy=0
move=0,0, -2*${d}
copy=1
move=0,0, -2*${d}
copy=2
move=0,0, -2*${d}
copy=3
move=0,0, -2*${d}
copy=4
move=0,0, -2*${d}
copy=5
move=0,0, -2*${d}
copy=6
move=0,0, -2*${d}

# BGO
copy=7
scale= ${e}/${c}, ${e}/${c}, ${v}/${w}
move=0,0,${d}/2

copy=-1
move=0,0,-${d}
