#! /usr/bin/env ./geometryfactor.py
# RPiRENA over Africa 2019

φ1=0
φ2=π/2
Δr=0.2
Δθ=1°
radius=12.5

# length units are mm, distances are center-center

# dimensions of the SSDs
define=wx=11.0
define=wy=21.0
define=wz=0.3
define=d=5.0

box=${wx}/2, ${wy}/2, ${wz}/2
move=0, 0, -${d}/2
copy=-1
move=0, 0, ${d}
