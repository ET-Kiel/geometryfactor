#! /usr/bin/env ./geometryfactor.py
# AHEPAM honey

φ1=0
φ2=π/12
Δr=1
Δθ=1°
radius=65

define=a=8.0
define=a₁=${a}/sqrt(3)
define=a₂=${a}*tan(π/12)
define=r₁=2/sqrt(3)*${a}
define=b=${a}*sqrt(21/6/sqrt(3)/tan(π/12))
define=b₁=${b}*cos(π/12)
define=b₂=${b}*sin(π/12)
define=Δc=5
define=c=(2*${b}-${a}+${Δc})
define=c₀=${c}/sqrt(2)
define=c₁=${c}*cos(π/6)
define=c₂=${c}*sin(π/6)
define=c₃=${c}*cos(π/12)
define=c₄=${c}*sin(π/12)
define=d=30.0
define=w=1.0
define=v=(${d}-10)
#define=e=(${c}-${a})
define=e=${c}

#radius=sqrt((2*${d}+${w})**2 + 2*${b}**2)

# center segment
detector
plane=    0, 0,-${w}/2;   1., 0, 0;   0, 1., 0
plane=    0, 0, ${w}/2;   0, 1., 0;   1., 0, 0
plane=   -${a}, 0, 0;   0, 1., 0;   0, 0,  1.
plane=    ${a}, 0, 0;   0, 0,  1.;  0, 1., 0
plane=   0,  ${a}, 0;   1., 0, 0;   0, 0,  1.
plane=   0, -${a}, 0;   0, 0,  1.;  1., 0, 0
plane=  0, -${r₁}, 0;  0, 0,  1.;  ${a},  ${a₁}, 0
plane=  0, -${r₁}, 0;  0, 0,  1.;  ${a}, -${a₁}, 0
plane=  0,  ${r₁}, 0;  ${a},  ${a₁}, 0;  0, 0,  1.
plane=  0,  ${r₁}, 0;  ${a}, -${a₁}, 0;  0, 0,  1.

plane=  -${r₁}, 0, 0;   ${a₁}, ${a}, 0;  0, 0,  1.
plane=  -${r₁}, 0, 0;  -${a₁}, ${a}, 0;  0, 0,  1.
plane=   ${r₁}, 0, 0;  0, 0,  1.;  ${a₁},  ${a}, 0
plane=   ${r₁}, 0, 0;  0, 0,  1.;  -${a₁}, ${a}, 0

# hex segment
detector
plane=    0, 0,-${w}/2;   1., 0, 0;   0, 1., 0
plane=    0, 0, ${w}/2;   0, 1., 0;   1., 0, 0
plane=    ${a}, 0, 0;   0, 1., 0;   0, 0,  1.
plane=    ${b}, 0, 0;   0, 0,  1.;  0, 1., 0
plane=    ${a}, -${a₂}, 0;  0, 0,  1.;  ${a}, -${a₂}, 0
plane=    ${a},  ${a₂}, 0;  ${a},  ${a₂}, 0;  0, 0,  1.
plane=    ${b₁}, -${b₂}, 0;  0, 0,  1.;  ${b₂}, ${b₁}, 0
plane=    ${b₁},  ${b₂}, 0;  ${b₂}, -${b₁}, 0;  0, 0,  1.

copy=-1
rz=π/6
copy=-2
rz=π/3
copy=-3
rz=π/2
copy=-4
rz=2*π/3
copy=-5
rz=5*π/6
copy=-6
rz=π
copy=-7
rz=7*π/6
copy=-8
rz=4*π/3
copy=-9
rz=3*π/2
copy=-10
rz=5*π/3
copy=-11
rz=11*π/6

# outer segment
detector
plane=   0, 0,-${w}/2;   1., 0, 0;   0, 1., 0
plane=   0, 0, ${w}/2;   0, 1., 0;   1., 0, 0
plane=  -${c}, 0, 0;   0, 1., 0;   0, 0,  1.
plane=   ${c}, 0, 0;   0, 0, 1.;   0, 1., 0
plane=  0, -${c}, 0;   0, 0, 1.;   1., 0, 0
plane=  0,  ${c}, 0;   1., 0, 0;   0, 0,  1.

plane=  -${c₀},  ${c₀}, 0;   1., 1., 0;   0, 0,  1.
plane=  -${c₀}, -${c₀}, 0;  -1., 1., 0;   0, 0,  1.
plane=   ${c₀},  ${c₀}, 0;   0, 0,  1.;  -1., 1., 0
plane=   ${c₀}, -${c₀}, 0;   0, 0,  1.;   1., 1., 0

plane=  -${c₁},  ${c₂}, 0;   ${c₂}, ${c₁}, 0;   0, 0,  1.
plane=  -${c₁}, -${c₂}, 0;  -${c₂}, ${c₁}, 0;   0, 0,  1.
plane=   ${c₁},  ${c₂}, 0;   0, 0, 1.;  -${c₂}, ${c₁}, 0
plane=   ${c₁}, -${c₂}, 0;   0, 0, 1.;   ${c₂}, ${c₁}, 0

plane=  -${c₃},  ${c₄}, 0;   ${c₄}, ${c₃}, 0;   0, 0,  1.
plane=  -${c₃}, -${c₄}, 0;  -${c₄}, ${c₃}, 0;   0, 0,  1.
plane=   ${c₃},  ${c₄}, 0;   0, 0, 1.;  -${c₄}, ${c₃}, 0
plane=   ${c₃}, -${c₄}, 0;   0, 0, 1.;   ${c₄}, ${c₃}, 0

plane=  ${c₂}, -${c₁}, 0;   0, 0, 1.;   ${c₁},  ${c₂}, 0
plane= -${c₂}, -${c₁}, 0;   0, 0, 1.;   ${c₁}, -${c₂}, 0
plane=  ${c₂},  ${c₁}, 0;   ${c₁}, -${c₂}, 0;   0, 0,  1.
plane= -${c₂},  ${c₁}, 0;   ${c₁},  ${c₂}, 0;   0, 0,  1.

plane=  ${c₄}, -${c₃}, 0;   0, 0, 1.;   ${c₃},  ${c₄}, 0
plane= -${c₄}, -${c₃}, 0;   0, 0, 1.;   ${c₃}, -${c₄}, 0
plane=  ${c₄},  ${c₃}, 0;   ${c₃}, -${c₄}, 0;   0, 0,  1.
plane= -${c₄},  ${c₃}, 0;   ${c₃},  ${c₄}, 0;   0, 0,  1.


copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}
copy=-14
move=0,0,${d}

copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}
copy=-28
move=0,0,-${d}

copy=0
move=0,0, 2*${d}
copy=1
move=0,0, 2*${d}
copy=2
move=0,0, 2*${d}
copy=3
move=0,0, 2*${d}
copy=4
move=0,0, 2*${d}
copy=5
move=0,0, 2*${d}
copy=6
move=0,0, 2*${d}
copy=7
move=0,0, 2*${d}
copy=8
move=0,0, 2*${d}
copy=9
move=0,0, 2*${d}
copy=10
move=0,0, 2*${d}
copy=11
move=0,0, 2*${d}
copy=12
move=0,0, 2*${d}

copy=0
move=0,0, -2*${d}
copy=1
move=0,0, -2*${d}
copy=2
move=0,0, -2*${d}
copy=3
move=0,0, -2*${d}
copy=4
move=0,0, -2*${d}
copy=5
move=0,0, -2*${d}
copy=6
move=0,0, -2*${d}
copy=7
move=0,0, -2*${d}
copy=8
move=0,0, -2*${d}
copy=9
move=0,0, -2*${d}
copy=10
move=0,0, -2*${d}
copy=11
move=0,0, -2*${d}
copy=12
move=0,0, -2*${d}

# BGO
copy=13
scale= ${e}/${c}, ${e}/${c}, ${v}/${w}
move=0,0,${d}/2

copy=-1
move=0,0,-${d}
