#! /usr/bin/python3

import numpy, sys

a = numpy.zeros((5,16,16))

θ=-1

for l in sys.stdin:
    ll=list(map(float, l.split()))
    if θ != ll[0]:
        θ = ll[0]
        sys.stderr.write("%.2g\r" % θ)
    if sum(ll[4:]) < 49:
        continue
    for i in range(16):
        if ll[i+4]>25:
            for j in range(16):
                if ll[j+20] > 25:
                    a[0,i,j] += 1
                    a[1,i,j] += ll[0]
                    a[2,i,j] += ll[0]*ll[0]
                    a[3,i,j] += ll[1]
                    a[4,i,j] += ll[1]*ll[1]

# TODO: apply symmetries for full 2π flux
                    
θ = a[1] / a[0]
σθ = numpy.sqrt((a[2]**2/a[0] - θ**2)/(a[0]-1))
φ = a[3] / a[0]
σφ = numpy.sqrt((a[4]**2/a[0] - φ**2)/(a[0]-1))

for i in range(16):
    for j in range(16):
        print(i, j, a[0,i,j], θ[i,j], σθ[i,j], φ[i,j,], σφ[i,j])

