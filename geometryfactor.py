#! /usr/bin/python3

from sys import argv, stdout, stderr
from math import pi as π
from math import atan, atan2
import numpy
from numpy import sqrt, sin, cos, tan, array, arange, empty, cross, newaxis
from numpy.linalg import solve
from getopt import getopt

verbose=0

def scale(x=1., y=1., z=1.):
    return array(((x,0,0),
                  (0,y,0),
                  (0,0,z)))

def rotate(axis, φ):
    m = scale()
    c = cos(φ)
    s = -sin(φ)
    for i in range(3):
        if i != axis:
            m[i,i]=c
            for j in range(3):
                if j != i and j != axis:
                    m[i,j] = s
                    s = -s
    return m

def vector(x=0., y=0., z=0.):
    return array([[x],[y],[z]])

def vtuple(v):
    return tuple(v[:,0])

def length(v):
    return sqrt(v.T @ v)[...,0,0]

class ray:
    "Basic ray impelmentation"
    
    def __init__(self, θ=0, φ=0, r=0, ψ=0):
        self.θ = θ
        self.φ = φ
        self.r = r
        self.ψ = ψ
        self.dir = rotate(2,φ) @ rotate(1,θ)
        self.o = self.dir @ vector(r*cos(ψ), r*sin(ψ))
        self.v = self.dir @ vector(z=1)

    def ov(self, o, v):
        self.o = o
        self.v = v
        return self
        
    def __repr__(self):
        return "ray(o=(%g,%g,%g), v=(%g,%g,%g))" % (
            vtuple(self.o)+vtuple(self.v))

class rays:
    "Fast reimplementation of a beam of rays"
    
    def __init__(self, r, Δr):
        nr = int(r/Δr + 1.001)
        self.n = 0
        rψ = 0
        for ir in range(nr):
            nnψ = 2*π*ir + 0.5 + rψ
            nψ = int(nnψ + 0.5)
            rψ = nnψ - nψ
            self.n += nψ
        self.xy = empty((self.n,3,1))
        self.r = empty((self.n,))
        self.ψ = empty((self.n,))
        i=0
        rψ = 0
        for ir in range(nr):
            r = ir*Δr
            nnψ = 2*π*ir + 0.5 + rψ
            nψ = int(nnψ + 0.5)
            rψ = nnψ - nψ
            dψ = 2*π/nψ
            for iψ in range(nψ):
                ψ = iψ * dψ + dψ/2
                self.r[i] = r
                self.ψ[i] = ψ
                self.xy[i,:,0] = (r*cos(ψ), r*sin(ψ), 0)
                i = i+1

    def bend(self, θ, φ):
        self.θ = θ
        self.φ = φ
        self.dir = rotate(2,φ) @ rotate(1,-θ)
        self.v = self.dir @ vector(z=1)
        self.o = (self.dir @ self.xy)[...,0].T
    
class plane:
    index=[0]
    
    def __init__(self, o, v, w):
        self.idx = self.index[0]
        self.index[0] += 1
        self.o = o
        self.v = v
        self.w = w
        self.normalize()

    def normalize(self):
        self.n = cross(self.v, self.w, axis=0).T
        self.M = empty((3,3))
        self.M[:,0:1]=self.v
        self.M[:,1:2]=self.w
        self.rays=(ray().ov(self.o, self.v),
                   ray().ov(self.o, self.w),
                   ray().ov(self.o+self.w, self.v),
                   ray().ov(self.o+self.v, self.w),
                   ray().ov(self.o, self.v+self.w),
                   ray().ov(self.o, self.v-self.w), )

    def transform(self, m):
        return plane(m @ self.o, m @ self.v, m @ self.w)
        
    def move(self, v):
        return plane(self.o+v, self.v, self.w)
        
    def inside(self, p):
        return (self.n @ (p-self.o))[0] >= -0.00001

    def intersection(self, r):
        self.M[:,2:3] = r.v
        try:
            c=solve(self.M, r.o-self.o)[...,2,:]
        except Exception as e:
            if verbose:
                stderr.write("solve: %s: %s\n" % (str(self.M), repr(e)))
            return -1, None
        return c, r.o - c*r.v

    def plane_intersection(self, other, limit=1e10, delta=1e-5):
        p = [ self.intersection(r)[1] for r in other.rays]
        p += [ other.intersection(r)[1] for r in self.rays]
        p = [(length(pp),pp) for pp in p if pp is not None]
        p.sort(key=lambda x: x[0])
        while p and p[-1][0] > limit:
            p[-1:]=[]
        while len(p) > 2:
            if length(p[1][1]-p[0][1]) < delta*p[1][0]:
                p[:1]=[]
                continue
            if p[1][0] < delta*p[-1][0]:
                p[:1]=[]
                continue
            break
        if len(p)<2:
            return None
        ry = ray().ov(p[0][1], p[1][1]-p[0][1])
        return ry
        
    def planes_intersection(self, other, *more):
        ry = self.plane_intersection(other)
        if not ry:
            return []
        p = [pl.intersection(ry)[1] for pl in more]
        return p
    
    def __repr__(self):
        return "plane[%d](o=(%g,%g,%g), v=(%g,%g,%g), w=(%g,%g,%g))" % (
            (self.idx,)+vtuple(self.o)+vtuple(self.v)+vtuple(self.w))

class detector(list):

    def hitplane(self, r, i):
        c, p = self[i].intersection(r)
        if p is None:
            return False, 0
        h = abs(c) < 1e10
        for j, pl in enumerate(self):
            if j != i:
                inside = pl.inside(p)
                h = numpy.logical_and(h, inside, out=h)
        return h, p

    def inside(self, p):
        h = (p.T[...,newaxis,:] @ p.T[...,newaxis])[...,0,0] < 1e20
        for pl in self:
            inside = pl.inside(p)
            h = numpy.logical_and(h, inside, out=h)
        return h

    def hit(self, r):
        h=[]
        for i in range(len(self)):
            f,p = self.hitplane(r, i)
            h.append((f, p))
        return h

    def pathlength(self, r):
        h=[p for f,p in self.hit(r) if f]
        if len(h)<2:
            return 0
        d = h[1]-h[0]
        return sqrt((d.T @ d)[0,0])

    def pathlength_beam(self, r):
        h=self.hit(r)
        c = [cc for cc,pp in h if cc is not None]
        p = [pp for cc,pp in h if cc is not None]
        p1 = numpy.select(c,p)
        c.reverse()
        p.reverse()
        p2 = numpy.select(c,p)
        d = p2 - p1
        return sqrt((d.T[...,newaxis,:] @ d.T[...,newaxis])[...,0,0])
    
    def transform(self, m):
        return detector([p.transform(m) for p in self])
        
    def move(self, v):
        return detector([p.move(v) for p in self])

    def corners(self):
        c=[]
        for i in range(len(self)-2):
            for j in range(i+1,len(self)-1):
                c.extend(self[i].planes_intersection(self[j], *self[j+1:]))
        c = array([cc for cc in c if cc is not None])[...,0].T
        c = c[:,self.inside(c)].T[...,newaxis]
        return c

    def edges(self):
        ee=[]
        for i in range(len(self)-1):
            for j in range(i+1, len(self)):
                ry = self[i].plane_intersection(self[j])
                pp=[]
                if ry is not None:
                    for k in range(len(self)):
                        if k!=i and k!=j:
                            c,p = self[k].intersection(ry)
                            if p is not None and length(p)<1e10 and self.inside(p):
                                pp.append(p)
                if len(pp) >= 2:
                    ee.append(pp)
        return ee

    def gnuplot(self, edjes=None):
        if edjes is None:
            edjes = self.edges()
        for e in edjes:
            stdout.write("%g %g %g\n%g %g %g\n\n\n" % (tuple(e[0])+tuple(e[1])))

    def planes(self):
        # is this useful?
        self.o = numpy.stack([p.o for p in self])
        self.n = numpy.stack([p.n for p in self])
        self.v = numpy.stack([p.v for p in self])
        self.w = numpy.stack([p.w for p in self])
        return self.o, self.n, self.v, self.w

deg=π/180

def box(x=1.0, y=1.0, z=1.0):
    return detector([
        plane(vector(z=-z), vector(x=1), vector(y=1)),
        plane(vector(z= z), vector(y=1), vector(x=1)),
        plane(vector(x=-x), vector(y=1), vector(z=1)),
        plane(vector(x= x), vector(z=1), vector(y=1)),
        plane(vector(y=-y), vector(z=1), vector(x=1)),
        plane(vector(y= y), vector(x=1), vector(z=1)),
    ])

def prism(n=6, a=1.0, z=1.0, φ=0.0):
    p = [
        plane(vector(z=-z), vector(x=1), vector(y=1)),
        plane(vector(z= z), vector(y=1), vector(x=1)),
    ]
    φ = 2*π * arange(int(n))/n + φ
    p.extend([
        plane(
            vector(a*c, a*s),
            vector(  s,  -c),
            vector(z=1)
        )
        for c,s in zip(cos(φ), sin(φ))
    ])
    return detector(p)

µM = box(41., 41., 13.)

def run(world, r, Δr, Δθ, θ1=0, θ2=π/2, φ1=0, φ2=2*π):
    fmt = "%g %g %g %g "+len(world)*" %g"+"\n"
    nr = int(r/Δr + 1.001)
    rφ = 0;
    for iθ in range(int((θ2-θ1)/Δθ + 1.001)):
        θ = θ1 + iθ * Δθ
        nnφ = (φ2-φ1)*(cos(θ)-cos(θ+Δθ))/Δθ**2 + rφ
        nφ = max(1, int(nnφ+0.5))
        rφ = nnφ - nφ
        Δφ = (φ2-φ1)/nφ
        for iφ in range(nφ):
            φ = φ1 + iφ * Δφ + Δφ/2
            rψ = 0
            for ir in range(nr):
                r = ir * Δr
                nnψ = 2*π*ir + 0.5 + rψ
                nψ = int(nnψ + 0.5)
                rψ = nnψ - nψ
                dψ = 2*π/nψ
                for iψ in range(nψ):
                    ψ = iψ * dψ + dψ/2
                    ry = ray(θ, φ, r, ψ)
                    pl = [d.pathlength(ry) for d in world]
                    stdout.write(fmt % ((θ, φ, r, ψ)+tuple(pl)))

def run_beam(world, r, Δr, Δθ, θ1=0, θ2=π/2, φ1=0, φ2=2*π):
    fmt = "%g %g %g %g "+len(world)*" %g"+"\n"
    beam = rays(r, Δr)
    b = None
    rφ = 0;
    for iθ in range(int((θ2-θ1)/Δθ + 1.001)):
        θ = θ1 + iθ * Δθ
        nnφ = (φ2-φ1)*(cos(θ)-cos(θ+Δθ))/Δθ**2 + rφ
        nφ = max(1, int(nnφ+0.5))
        rφ = nnφ - nφ
        Δφ = (φ2-φ1)/nφ
        for iφ in range(nφ):
            φ = φ1 + iφ * Δφ + Δφ/2
            beam.bend(θ, φ)
            pl = [d.pathlength_beam(beam) for d in world]
            for i in range(beam.n):
                bb=[θ, φ, beam.r[i], beam.ψ[i]]
                ll = tuple(bb+[l[i] for l in pl])
                stdout.write(fmt % ll)
        
test_rays = [
    ("x-axis", ray().ov(vector(),vector(x=1))),
    ("y-axis", ray().ov(vector(),vector(y=1))),
    ("z-axis", ray().ov(vector(),vector(z=1))),
]

def test(det, rays):
    for name, ry in rays:
        pl = [d.pathlength(ry) for d in det]
        stderr.write(("test %s:"+len(pl)*" %g"+"\n") % ((name,)+tuple(pl)))

def corners(world):
    c = [d.corners() for d in world]
    ll=[]
    for i,d in enumerate(c):
        stderr.write("Detector[%d]\n" %i)
        for cc in d:
            if cc is not None:
                l = length(cc)
                ll.append(l)
                stderr.write(" |%g, %g, %g| = %g\n" % (cc[0,0],cc[1,0],cc[2,0],l))
    max_r = max(ll)
    return max_r, c
        
short_opt = "hSNTREGU:D:C:B:P:µMr:d:a:p:x:"
long_opt = ["help",
            "φ1=", "φ2=", "θ1=", "θ2=", "φ₁=", "φ₂=", "θ₁=", "θ₂=",
            "Δθ=", "µMustang", "test", "size", "run", "corners", "gnuplot",
            "radius=", "resolution=", "Δr=" ,
            "detector", "copy=", "select=", "box=", "prism=",
            "plane=", "ray=",
            "rx=", "ry=", "rz=", "move=", "scale=",
            "define=", "replace=", "undefine="]

args = argv[1:]
options = []
while args:
    oo, aa = getopt(args, short_opt, long_opt)
    args = []
    options.extend(oo)
    if aa:
        fn = aa[0]
        with open(fn) as f:
            for l in f:
                l=l.strip()
                if not l:
                    continue
                if l[0]=='-':
                    args.append(l)
                elif l[0]=='@':
                    args.append(l[1:])
                elif l[0] != "#":
                    args.append("--"+l)
        args.extend(aa[1:])

Δr = 1.0
Δθ = 1*deg
r = 0
world = [detector()]
θ1=0
θ2=π/2
φ1=0
φ2=2*π/8
do_size = False
do_test = False
do_run = False
do_corners = False
do_gnuplot = False
rot = rotate(0,0)
ori = vector()

macros={}

def aval(v):
    vv=""
    while v!=vv and v.find("$") >= 0:
        vv=v
        for m in macros:
            v = v.replace(m, macros[m])
    v = v.replace("°", "*deg")
    return eval(v)

def avec(v):
    v = v.strip().replace(",", " ")
    return tuple([aval(vv) for vv in v.split()])

for o,v in options:

    if o in "-h --help":
        with open("geometryfactor.md") as f:
            stderr.write(f.read())
        raise SystemExit(0)
    
    if o in "-µ -M --µMustang":
        if world[-1]:
            world.append(µM)
        else:
            world[-1] = µM
    if o in "-p --plane":
        world[-1].append(plane(*tuple([vector(*avec(vv)) for vv in v.split(";")])))
    if o in "-x --ray":
        vv = v.split(";")
        if len(vv)==2:
            test_rays.append((vv[0], ray(*avec(vv[1]))))
        else:
            test_rays.append((vv[0], ray().ov(vector(*avec(vv[1])), vector(*avec(vv[2])))))
    if o in "-a --Δθ":
        Δθ = aval(v)
    if o in "-d --Δr --resolution":
        Δr = aval(v)
    if o in "--θ1 --θ₁":
        θ1 = aval(v)
    if o in "--θ2 --θ₂":
        θ2 = aval(v)
    if o in "--φ1 --φ₁":
        φ1 = aval(v)
    if o in "--φ2 --φ₂":
        φ2 = aval(v)
    if o in "-r --radius":
        r = aval(v)
    if o in "-S --size":
        do_size=True
    if o in "-T --test":
        do_test=True
    if o in "-R --run":
        do_run=True
    if o in "-G --gnuplot":
        do_gnuplot=True
    if o in "-E --corners":
        do_corners=True
    if o in "--rx":
        world[-1] = world[-1].transform(rotate(0, aval(v)))
    if o in "--ry":
        world[-1] = world[-1].transform(rotate(1, aval(v)))
    if o in "--rz":
        world[-1] = world[-1].transform(rotate(2, aval(v)))
    if o in "--move":
        world[-1] = world[-1].move(vector(*avec(v)))
    if o in "--scale":
        world[-1] = world[-1].transform(scale(*avec(v)))
    if o in "-N --detector":
        if world[-1]:
            world.append(detector())
    if o in "-C --copy --select":
        if not world[-1]:
            world[-1:] = []
        idx=int(v)
        world.append(world[idx])
        if o in "--select":
            world[idx:idx+1]=[]
    if o in "-B --box":
        if not world[-1]:
            world[-1:] = []
        world.append(box(*avec(v)))
    if o in "-P --prism":
        if not world[-1]:
            world[-1:] = []
        world.append(prism(*avec(v)))
        
    if o in "-U --undefine -D --define --replace":
        vv=v.split("=",1)
        k = "${"+vv[0]+"}"
        if o in "-U --undefine":
            if k in macros:
                stderr.write("undefine macro %s=%s\n" % (k,macros[k]))
                del macros[k]
        elif o in "--replace" or not k in macros:
            if k in vv[1]:
                raise ValueError("recursive macro %s" % v)
            macros[k]=vv[1]
            stderr.write("define macro %s=%s\n" % (k,vv[1]))

if not world[-1]:
    world[-1:]=[]
if not world:
    world=[µM]

if not r or do_corners:
    max_r, c = corners(world)
    if not r:
        r = max_r

if do_gnuplot:
    for d in world:
        d.gnuplot()

if do_size:
    nr = int(r/Δr+1.001)
    no = π*nr**2 - (π-0.5)*nr
    sr = (φ2-φ1)*(cos(θ1)-cos(θ2+Δθ))
    A = π*((nr-0.5)*Δr)**2
    nv = sr/Δθ**2
    nn = int(nv)*int(no)
    stderr.write("""Run size estimate:
detectors: %d  planes: %d
r = %g
Δr = %g
Δθ = %g = %g°, θ=%g…%g φ=%g…%g
ω = %g sr = %g π sr
A = %g ⧠
n = %.0f × %.0f = %.0f = %g /(sr ⧠)
""" % (
    len(world), sum([len(d) for d in world]),
    r, Δr,
    Δθ, Δθ/deg, θ1, θ2, φ1, φ2,
    sr, sr/π, A,
    nv, no, nn, nn/sr/A 
))

if do_test:
    test(world, test_rays)
if do_run:
    run_beam(world, r, Δr, Δθ, θ1, θ2, φ1, φ2)
